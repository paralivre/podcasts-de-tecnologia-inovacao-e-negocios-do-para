# Podcasts de Tecnologia, Inovação, Empreendorismo e Negócios do Pará
=====================================================================

Os podcasts paraenses de Tecnologia, Inovação e Negócios estão surgindo com toda força em nossa região. Esse repositório contém uma lista organizada dos podcasts dividos em suas determinadas áreas.

## O que são podcasts?

Podcasts são programas gravados onde uma ou mais pessoas argumentam sobre um assunto em comum. Geralmente estes episódios vão ao ar nas plataformas de streaming e, esporadicamente, também são lançados no YouTube. 

Os episódios podem ter apresentadores fixos e receber visitantes para conversar a respeito de um assunto específico.

## Para realizar pedidos de novos podcasts:

1. Crie um pull request para adicionar novo podcast;
2. Retire o :triangular_flag_on_post: do podcast com esse ícone e coloque no novo podcast adicionado;
3. Avise no grupo do [ParaLivre no Telegram](http://t.me/paralivre) a alteração.
**OBS**: Caso não saiba usar o GitLab, basta pedir para que seu podcast seja adicionado aqui no grupo do telegram do [ParaLivre](http://t.me/paralivre). 

# Tecnologia e Inovação
- **FacompCast**: O podcast da [@computacaoufpa](https://www.instagram.com/computacaoufpa) com apoio da Pro-Reitoria de Extensão da UFPA.
Hosts: [@heycampoos](https://www.instagram.com/heycampoos), [@christiandejesus23](https://www.instagram.com/christiandejesus23) e [@drvictorsantiago](https://www.instagram.com/drvictorsantiago) 
  - [Spotify](https://open.spotify.com/show/7oGkFKS4eV4WM2XjbEFeJu)
  - [RSS](https://anchor.fm/s/583b35c8/podcast/rss)  
  - [Instagram](https://www.instagram.com/facompcast) 
  - [Twitter](https://twitter.com/facompcast) 

- **Maker Cast**: A voz da Periferia da Amazônia. Este podcast é produzido pela equipe do [Projeto Inovação Gileade](https://inovacaogileade.com.br)
  - [Youtube](https://www.youtube.com/@makercast)  

- **Se Liga Tech!**: Muita informação através de entrevistas com personalidades do ramo da tecnologia, bate papo descontraído sobre curiosidades e novidades do segmento.
  - [Spotify](https://open.spotify.com/show/0BnW9efhZL7sZ7VntdEjpC)
  - [Site](https://www.oliberal.com/play/podcast/se-liga-tech) 
  - [Google Podcasts](https://podcasts.google.com/feed/aHR0cHM6Ly9hbmNob3IuZm0vcy84ODVkN2UxNC9wb2RjYXN0L3Jzcw)  
  - [Apple Podcasts](https://podcasts.apple.com/us/podcast/se-liga-tech/id1613386628)  
  - [Castbox](https://castbox.fm/channel/id4823397?country=br)
  - [Amazon Music](https://music.amazon.com.br/podcasts/d91605d3-3651-47b5-9cf1-15450f9adb32/se-liga-tech)
  - [Deezer](https://www.deezer.com/br/show/3489607)  

- **Debug Podcast**: É o lugar onde a mente dos estudantes de tecnologia é "debugada" e preparada para enfrentar os desafios da área. Os episódios são projetados para ajudar a descobrir soluções que a tecnologia pode prover para os problemas complexos, desde as principais linguagens de programação até o uso de Inteligência Artificial. Com entrevistas, debates e insights valiosos de especialistas da área, o Debug Podcast é o seu canal para decodificar e entender a área de tecnologia. 
  - [Instagram](https://www.instagram.com/debugpdc) 
  - [Youtube](https://www.youtube.com/@debugpdc)    

# Negócios e Empreendorismo
- **Belém Negócios**: [Rodrigo Souza](https://www.instagram.com/rodrigosoupe), fundador do Belém Negócios, entrevista os maiores empreendedores paraenses. 
  - [Spotify](https://open.spotify.com/show/7oGkFKS4eV4WM2XjbEFeJu)
  - [Youtube](https://www.youtube.com/@belemnegocios8223)  
  - [Site](https://www.belemnegocios.com/podcast)
  - [Instagram](https://www.instagram.com/belemnegocios.com.br)


- **O Jogo Podcast**: Quem não joga, não vence! Empreendedorismo, Negócios, Empresas e Networking.
Hosts: [Luis Duarte](https://www.instagram.com/luisrsduarte) e [Evandro Paes](https://www.instagram.com/evandrompaes) 
  - [Youtube](https://www.youtube.com/@ojogopodcast)  
  - [Instagram](https://www.instagram.com/ojogopodcast)


- **Bora Empreender Podcast**: Podcast, Empreendedorismo, Intraempreendedor e Motivação.
Hosts: [Fabricio Cruz](https://www.instagram.com/fabriciocruz1) e [Tiago Fernandes](https://www.instagram.com/tiagofernandesauge) 
  - [Youtube](https://www.youtube.com/@BoraEmpreenderPodcast)  
  - [Instagram](https://www.instagram.com/boraempreender_podcast)

- **A Dupla Podcast**: Bate-papo sobre negócios, marketing e desenvolvimento pessoal, sempre acompanhados de boas histórias e inspirações valiosas.
Hosts: [Otavio de Oliveira](https://www.instagram.com/otaviorodrigo84) e [Tamires Ishimori](https://www.instagram.com/tamires.ishimori) 
  - [Youtube](https://www.youtube.com/@ADuplaPodcast)  
  - [Instagram](https://www.instagram.com/aduplapodcast/)

- **Podcast Segue o Plano**: Qual é a tua melhor versão? É a que te ensinaram ou a que queres construir? Todo dia 01, 10 e 20, eu, Ricardo Polaro, e parceiros, clientes, empreendedores, gente criativa e inovadora, vamos falar de negócios. Segue o Plano, o Podcast que abre a mente de quem quer empreender e de quem já vive o mundo dos negócios na Amazônia.
Host: [Ricardo Polaro](https://www.instagram.com/ricardopolaro) 
  - [Spotify](https://open.spotify.com/show/1WDKf4WnHrVLrZukF9vqZg)  
  - [Instagram](https://www.instagram.com/podcastsegueoplano/)
  - [Youtube](https://www.youtube.com/@RicardoPolaro)  

- **PatriCast Papo Empreendedor**: O seu Podcast sobre empreendedorismo.
Hosts: [Gilson Dias](https://www.instagram.com/gfdiascontador/) e [Julio Cezar Durans](https://www.instagram.com/_juliodurans_/)
  - [Youtube](https://www.youtube.com/@PatriCastPapoEmpreendedor)  
  - [Instagram](https://www.instagram.com/contabilidade.patrimonio)